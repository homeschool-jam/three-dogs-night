﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementController : MonoBehaviour
{

    public float moveSpeed = 12.5f;
    public float acceleration = 1.15f;
    public float stoppingTime = .25f;
    public float groundedDist = .05f;
    public float jump = 20f;
    public float climb = 10f;
    public float fastFall = .75f;

    public Rigidbody rb;
    public Collider col;
    private float refLRSpeed;
    private float colliderHeight;
	public CameraSwap cameraswap;
	public bool canClimb = false;
	public bool climbing = false;

    void Start()
    {
        colliderHeight = col.bounds.extents.y;
    }

    void Update()
    {
		if (cameraswap.getFocus() == this.gameObject && !cameraswap.getIsSwapping()) {
			float lrSpeed = rb.velocity.z;
			float vSpeed = rb.velocity.y;


			if (Input.GetAxis("Horizontal") > 0) {
				lrSpeed += acceleration;
			}
			else if (Input.GetAxis("Horizontal") < 0) {
				lrSpeed -= acceleration;
			}
			else {
				lrSpeed = Mathf.SmoothDamp(lrSpeed, 0f, ref refLRSpeed, stoppingTime);
			}

			lrSpeed = Mathf.Clamp(lrSpeed, -moveSpeed, moveSpeed);

			bool grounded = isGrounded();

			if(canClimb && Input.GetKeyDown(KeyCode.W)) {
				climbing = true;
			}

			if (grounded && !climbing) {
				if (Input.GetButtonDown("Jump")) {
					vSpeed = jump;
				}
			}
			else if(!climbing){
				vSpeed -= fastFall;
			}
			else if (Input.GetKey(KeyCode.W)) {
				vSpeed = climb;
			}
			else {
				vSpeed = -climb;
			}

			

			rb.velocity = new Vector3(0f, vSpeed, lrSpeed);

            if(Input.GetKeyDown(KeyCode.Escape))
            {
				Application.Quit();
            }
		}
    }

	private void FixedUpdate()
	{
		
	}

	bool isGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, colliderHeight+groundedDist);
    }

	public void OnTriggerEnter(Collider other)
	{
		if(other.tag == "climbable" && this.name == "Cat") {
			canClimb = true;
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "climbable" && this.name == "Cat") {
			canClimb = false;
			climbing = false;
		}
	}


}
