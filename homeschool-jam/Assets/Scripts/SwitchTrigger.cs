﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This is a class for when players reach a switch and it does something
public class SwitchTrigger : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        //this makes the switch respond
        if (other.gameObject.tag == "Player")
        {
            this.GetComponent<MeshRenderer>().material.color = Color.blue;
        }
    }

}
