﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwap : MonoBehaviour
{
	public Camera camera;
	public GameObject player1;
	public GameObject player2;
	public GameObject player3;
	private GameObject focus;
	private Queue<GameObject> playerList;

	public float cameraDistance = 20;
	public float speed = 1.0F;
	private float startTime;
	private float journeyLength;
	private bool isSwapping;
	private Coroutine currentCoroutine;
	public float cameraHeight = 5;

	// Start is called before the first frame update
	void Start()
    {
		focus = player1;
		playerList = new Queue<GameObject>();
		playerList.Enqueue(player2);
		playerList.Enqueue(player3);
		currentCoroutine = StartCoroutine(cameraSwap());

	}

    // Update is called once per frame
    void Update()
    {
		//focus.transform.Translate(.01f, 0, 0);
		if (Input.GetMouseButtonDown(0)) {
			playerList.Enqueue(focus);
			focus = playerList.Dequeue();
			StopCoroutine(currentCoroutine);
			currentCoroutine = StartCoroutine(cameraSwap());
		}
		else if (!isSwapping) {
			camera.transform.position = new Vector3(cameraDistance, focus.transform.position.y + cameraHeight, focus.transform.position.z);
		}
    }

	IEnumerator cameraSwap()
	{
		isSwapping = true;
		startTime = Time.time;
		journeyLength = Vector3.Distance(camera.transform.position, new Vector3(cameraDistance, focus.transform.position.y + cameraHeight, focus.transform.position.z));
		float distCovered = 0;
		while (Vector3.Distance(camera.transform.position, new Vector3(cameraDistance, focus.transform.position.y + cameraHeight, focus.transform.position.z)) >= 0.1) {
			distCovered = (Time.time - startTime) * speed;
			float fractionOfJourney = distCovered / journeyLength;
			camera.transform.position = Vector3.Lerp(camera.transform.position, new Vector3(cameraDistance, focus.transform.position.y + cameraHeight, focus.transform.position.z), fractionOfJourney);
			yield return new WaitForFixedUpdate();
		}
		isSwapping = false;
	}

	public GameObject getFocus()
	{
		return focus;
	}

	public bool getIsSwapping()
	{
		return isSwapping;
	}
}
