﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyObject : MonoBehaviour
{
	public GameObject obstacle;

	public void OnTriggerEnter(Collider other)
	{
		Destroy(obstacle);
		Destroy(this.gameObject);
	}
}
