﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//This class works to load the next scene after all three players have crossed
public class LoadSceneOnTrigger : MonoBehaviour
{
    private int playerCount; //keep count of the three player characters

    public string levelToLoad; //the next Scene to Load
   
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playerCount++;
            if(playerCount == 3)
            {
                LoadNextScene();
            }
        }
    }

    void OnTriggerExit()
    {
        playerCount = 0;
    }

    void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }
}


