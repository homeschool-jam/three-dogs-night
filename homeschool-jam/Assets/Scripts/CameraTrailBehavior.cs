﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrailBehavior : MonoBehaviour
{

    public Transform target;
    public float trailDistance = 2.5f;
    public float trailTime = .5f;

    private Vector2 refPos;

    // Update is called once per frame
    void Update()
    {
        Vector2 pos = new Vector2(transform.position.z, transform.position.y);
        Vector2 tarPos = new Vector2(target.position.z, target.position.y);

        if (Vector2.Distance(pos, tarPos) > trailDistance)
        {
            pos = Vector2.SmoothDamp(pos, tarPos, ref refPos, trailTime);
        }

        transform.position = new Vector3(transform.position.x, pos.y, pos.x);
    }
}
